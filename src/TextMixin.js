export const textMixin = {
    data(){
        return {
           text :'mixinm text'
        };
    },
    computed:{
        mixinReverse(){
            return this.text.split("").reverse().join("");
        },
        mixinLength(){
            let count =this.text.length;
            return this.text + ' ('+count+')';
        }
    },
};